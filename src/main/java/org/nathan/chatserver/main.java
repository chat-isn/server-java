package org.nathan.chatserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.java_websocket.WebSocket;

public class main {

	public static void main(String[] args) throws InterruptedException , IOException  {
		
		int port = 80;
		try {
			port = Integer.parseInt( args[ 0 ] );
		} catch ( Exception ex ) {
		}
		ChatServer s = new ChatServer( port );
		s.start();
		System.out.println( "ChatServer started on port: " + s.getPort() );
		BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
	
	}

}
