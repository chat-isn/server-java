package org.nathan.chatserver;

import java.io.IOException;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.nathan.chatserver.ChatServer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import org.nathan.chatserver.ChatServer.message.error;
import org.nathan.chatserver.ChatServer.message.fetchchatid;
import org.nathan.chatserver.ChatServer.message.hello;
import org.nathan.chatserver.ChatServer.message.login;
import org.nathan.chatserver.ChatServer.message.userdata;
import org.nathan.chatserver.ChatServer.message.chatid;
import org.nathan.chatserver.ChatServer.message.convlist;
import org.nathan.chatserver.ChatServer.message.msg;
import org.nathan.chatserver.ChatServer.message.fetchmessages;
import org.nathan.chatserver.ChatServer.message.fetchmaxmessages;


public class ChatServer extends WebSocketServer {
	
	Connection con = null;
	
	
	HashMap<Integer, WebSocket> Connections = new HashMap<Integer, WebSocket>();
	
	boolean isLogged = true;
	int userid = 0;

	public ChatServer( int port ) throws UnknownHostException {
		super( new InetSocketAddress( port ) );
	}

	public ChatServer( InetSocketAddress address ) {
		super( address );
	}

	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake ) {
	/**	System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the room!" );
		final dh server = new dh();
		server.generateKeys();
		String publicKeyString = javax.xml.bind.DatatypeConverter.printHexBinary(server.getPublicKey().getEncoded());
		conn.send(publicKeyString);
		server.generateCommonSecretKey();
		*/
	}

	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
		int useridd = 0;
		System.out.println( conn + " has left the room!" );
		for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
            if (entry.getValue().equals(conn)) {
                useridd = entry.getKey();
            }
		}
        Connections.remove(useridd);
		System.out.println(Connections.containsValue(conn));
	}

	@Override
	public void onMessage( WebSocket conn, String message ) {
		System.out.println( conn + ": " + message );
		int id = 0;
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		JsonReader reader = new JsonReader(new StringReader(message));
		String name = "";
	    String requete = "";
	    
		try {
			
			reader.beginObject();
			
		    while(reader.hasNext()){
		    	
		        JsonToken nextToken = reader.peek();
		        System.out.println(nextToken);
		         
		        name = reader.nextName();
		        
		        if(name.equals("id")) {
		        	id = reader.nextInt();
		        	System.out.println(id);
		        } else if(name.equals("type")) {
		        	String type = reader.nextString();
		        	
		        	if(type.equals("hello")) {
		        		System.out.println("Hello");
		        		final hello msg = new hello(id,"hello");
				        final String json = gson.toJson(msg);
				        conn.send(json);
		        	} else if(type.equals("arg")) {
		        			
		            	name = reader.nextName();
	        			int receiveduserid = reader.nextInt();
	        			name = reader.nextName();
	        			int receivedchatID = reader.nextInt();
	        			name=reader.nextName();
	        			String receivedMessage = reader.nextString();
	        			name=reader.nextName();
	        			String userlist = reader.nextString();
	        			
	        			boolean correctuser = false;

	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}
	        			
		        		if(isLogged && correctuser) {

    						String[] userids = userlist.split("-");
    						
    						
    		        		for(int i = 0; i < userids.length; i++) {
    		        			System.out.println(userids[i]);
    		        			if (Connections.containsKey(Integer.parseInt(userids[i]))) {
    		        				final msg msg = new msg(id,"receivedarg",receivedchatID, receiveduserid, receivedMessage);
        		        			final String json = gson.toJson(msg);
    		        				Connections.get(Integer.parseInt(userids[i])).send(json);
    		        			}
    		        		}
		        			
		        		} else {
		        			final error msg = new error(id,"error",256);
		        			final String json = gson.toJson(msg);
		        			conn.send(json);
		        			
		        		}
		        	} else if(type.equals("register")) {
		        		
		        		name = reader.nextName();
		        		String username = reader.nextString();
		        		name = reader.nextName();
		        		String email = reader.nextString();
		        		name = reader.nextName();
		        		String password = reader.nextString();
		        		
		        		boolean userexist = false;
		        		boolean emailexist = false;
		        		
		        		try {
		        			requete = "SELECT username FROM user";
		        			Statement stmt = con.createStatement();
		        			ResultSet rs;
		        			
		        			rs = stmt.executeQuery(requete);
		        			while(rs.next()) {
		        		
		        				String lastUsername =rs.getString("username");
		        				System.out.println(lastUsername);
		        				if(lastUsername.equals(username)) {
		        					userexist = true;
		        				}
		        			}	        			
		        		} catch(SQLException e) {
		        			e.printStackTrace();
		        		}
		        		
		        		try {
		        			String requetem = "SELECT email FROM user";
		        			Statement statemail = con.createStatement();
		        			ResultSet rs;
		        			
		        			rs = statemail.executeQuery(requetem);
		        			while(rs.next()) {
		        		
		        				String lastEmail = rs.getString("email");
		        				System.out.println(lastEmail);
		        				if(lastEmail.equals(email)) {
		        					emailexist = true;
		        				}
		        			}	        			
		        		} catch(SQLException e) {
		        			e.printStackTrace();
		        		}
		        		
		        		if(userexist) {
	        				System.out.println("Same username");
        					final error msg = new error(id,"error",114);
        					final String json = gson.toJson(msg);
        					conn.send(json);
	        			} 
		        		else if(emailexist){
		        			System.out.println("Same email");
        					final error msg = new error(id,"error",115);
        					final String json = gson.toJson(msg);
        					conn.send(json);
		        		} else {
		        			
	        				try {
	    		        		String request = "INSERT INTO user(username,email,password) VALUES ('" + username + "','" + email + "','" + password + "')";
	    		        		Statement stat = con.createStatement();
	    		        	    stat.executeUpdate(request);
	    		        	    System.out.println("Successfully added user"); 
	    		        	    
	    		        	    String requeteid = "SELECT userid FROM user WHERE email = '" + email + "'";
	        					String requeteusername = "SELECT username FROM user WHERE email = '" + email + "'";
	        					
	        					Statement stateid = con.createStatement();
	        					Statement stateusername = con.createStatement();
	        					
	        					ResultSet sid;
	        					ResultSet susername;
	        					
	        					sid = stateid.executeQuery(requeteid);
	        					susername = stateusername.executeQuery(requeteusername);
	        					
	        					sid.next();
	        					susername.next();
	        					
	        					
	        					userid = sid.getInt("userid");
	        					String fetchedusername = susername.getString("username");
	        					
	        					System.out.println("Successfully logon");
	        					final login msg = new login(id,"login", userid, fetchedusername);
	        					final String json = gson.toJson(msg);
	        					
	        					
	        					isLogged = true;
	        					
	        					Connections.put(userid,conn);
	        					conn.send(json);
	    		        	    
	    		        			
	    		        	} catch(SQLException e) {
	    		        		e.printStackTrace();
	    		        	}
	        			}
		        		
		        	 } else if(type.equals("login")) {
		        		
		        		JsonToken token = reader.peek();
		 		        System.out.println(token);
		        		
		        		name = reader.nextName();
		        		String email = reader.nextString();
		        		name = reader.nextName();
		        		
		        		String password = reader.nextString();
		        		
		        		try {
		        			String requetem = "SELECT email FROM user";
		        			String requetep = "SELECT password FROM user";
		        			
		        			Statement statepass = con.createStatement();
		        			ResultSet spass;
		        			
		        			Statement statemail = con.createStatement();
		        			ResultSet semail;
		        			
		        			semail = statemail.executeQuery(requetem);
		        			spass = statepass.executeQuery(requetep);
		        			
		        			boolean userexist = false;
		        			
		        			while(semail.next() && spass.next()) {
		        				String lastEmail = semail.getString("email");
		        				String lastPass = spass.getString("password");
		        				
		        				if(lastEmail.equals(email) && lastPass.equals(password)) {
		        					userexist = true;
		        				}
		        			}
		        			
		        			if(userexist) {
		        				String requeteid = "SELECT userid FROM user WHERE email = '" + email + "'";
	        					String requeteusername = "SELECT username FROM user WHERE email = '" + email + "'";
	        					
	        					Statement stateid = con.createStatement();
	        					Statement stateusername = con.createStatement();
	        					
	        					ResultSet sid;
	        					ResultSet susername;
	        					
	        					sid = stateid.executeQuery(requeteid);
	        					susername = stateusername.executeQuery(requeteusername);
	        					
	        					sid.next();
	        					susername.next();
	        					
	        					
	        					userid = sid.getInt("userid");
	        					String username = susername.getString("username");
	        					
	        					System.out.println("Successfully logon");
	        					final login msg = new login(id,"login", userid, username);
	        					final String json = gson.toJson(msg);
	        					
	        					
	        					isLogged = true;
	        					
	        					Connections.put(userid,conn);
	        					conn.send(json);
	        					
		        			} else {
		        				final error msg = new error(id,"error",117);
			        			final String json = gson.toJson(msg);
			        			conn.send(json);
		        			}
		        			
		        		} catch(SQLException e) {
		        			e.printStackTrace();
		        		}
		        	} else if (type.equals("createconv")){
		        		
		        			
			        		if(isLogged) {
		        			     
			        		name = reader.nextName();

			        		String userlist = reader.nextString();
			        		
			        		try {
			        			
		        				String request = "INSERT INTO conversation(userid) VALUES ('" + userlist + "')";
	    		        		Statement stat = con.createStatement();
	    		        	    stat.executeUpdate(request);
	    		        	    
	    		        	    String requetechatid = "SELECT LAST_INSERT_ID()";
	    		        	    Statement statt = con.createStatement();
	    		        	    ResultSet rchatid;
	    		        	    
	    		        	    rchatid = statt.executeQuery(requetechatid);
	    		        	    int chatid = 0;
	    		        	    if (rchatid.next()) {
	    		        	    	chatid = rchatid.getInt(1);
	    		        	    }
	    		        	    
	    		        	    String createtablerequest = "CREATE TABLE `" + chatid + 
			        	                   "` (messageid INT(255) NOT NULL AUTO_INCREMENT, " +
			        	                   "timestamp BIGINT, " + 
			        	                   "userid INT(255), " + 
			        	                   "message TEXT, " +
			        	                   "PRIMARY KEY ( messageid ))";
	    		        	    
	    		        	    Statement statement = con.createStatement();
	    		        	    statement.executeUpdate(createtablerequest);
	    		        	    
	    		        	    String[] userids = userlist.split("-"); 
	    		        	    
	    		        	    for(int i = 0; i < userids.length; i++) {
	    		        			System.out.println(userids[i]);
	    		        			if (Connections.containsKey(Integer.parseInt(userids[i]))) {
	    		        				final chatid msg = new chatid(id,"createconv", chatid);
	    			        			final String json = gson.toJson(msg);
	    		        				Connections.get(Integer.parseInt(userids[i])).send(json);
	    		        			}
	    		        		}
	    		        	    
	    		        	    
	    		        	    
		        				
		        			} catch(SQLException e) {
		        				e.printStackTrace();
		        			}
			        		
		        		}
		        	} else if (type.equals("deletechat")){
		        		
		        		name = reader.nextName();
		        		int receiveduserid = reader.nextInt();	
		        		name = reader.nextName();
		        		int chatid = reader.nextInt();
		        		
		        		
		        		boolean correctuser = false;
		        		boolean userinlist = false;
		        		
		        		try {
		        			
		        			String requeteconv = "SELECT userid FROM conversation WHERE chatid = '" + chatid + "'";
		        			Statement stat = con.createStatement();
		        			ResultSet sconvlist;
		        			sconvlist = stat.executeQuery(requeteconv);
		        			
		        			sconvlist.next();
		        			
		        			String userlist = sconvlist.getString("userid");
		        			String[] ids = userlist.split("-");
		        			
		        			for(int i = 0 ; i<ids.length; i++) {
	        					if(Integer.parseInt(ids[i]) == userid) {
	        						
	        						String sqluserid = ids[i];
		        					
	        						if(sqluserid.equals(String.valueOf(receiveduserid))) {
	        							userinlist = true;
	        						}
	        						
		        				}
	        				}
		        			
		        			
		        			
		        		} catch(SQLException e ) {
		        			e.printStackTrace();
		        		}
		        		
	        			
	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}
	        			
	        			
	        			
		        		if(isLogged && correctuser && userinlist) {
		        			
		        		try {
	        				
							String requestid = "SELECT userid FROM conversation WHERE chatid = '" + chatid + "'";
		        			Statement state = con.createStatement();
		        			ResultSet rs;
		        			rs = state.executeQuery(requestid);        			
		        			rs.next();
		        			
		        			String useridlist = rs.getString("userid");
		        			String[] userids = useridlist.split("-");
		        			
		        			
	        				String request = "DELETE FROM conversation WHERE chatid = '" + chatid + "'";
    		        		Statement stat = con.createStatement();
    		        	    stat.executeUpdate(request);
	        				
    		        	    for(int i = 0; i < userids.length; i++) {
    		        			System.out.println(userids[i]);
    		        			if (Connections.containsKey(Integer.parseInt(userids[i]))) {
    		        				final chatid msg = new chatid(id,"deletechat", chatid);
    			        			final String json = gson.toJson(msg);
    		        				Connections.get(Integer.parseInt(userids[i])).send(json);
    		        			}
    		        		}    
	        			} catch(SQLException e) {
	        				e.printStackTrace();
	        			}
		        		
	        		}
	        	} else if(type.equals("listconv")) {
		        		
		        		
		        		name = reader.nextName();
	        			int receiveduserid = reader.nextInt();
		        		boolean correctuser = false;
	        			
	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}

		        		if(isLogged && correctuser) {
		        			try {
		        				
		        			String requeteconv = "SELECT * FROM conversation";
		        			Statement stat = con.createStatement();
		        			ResultSet sconvlist;
		        			sconvlist = stat.executeQuery(requeteconv);
		        			
		        			List<String> chatids = new ArrayList<String>();
		        			
		        			while(sconvlist.next()) {
		        				String lastconv = sconvlist.getString("userid");
		        				String[] ids = lastconv.split("-");
		        				int lastchatid = sconvlist.getInt("chatid");
		        				
		        				for(int i = 0 ; i<ids.length; i++) {
		        					if(Integer.parseInt(ids[i]) == userid) {
		        						
		        						int chatid = lastchatid;
			        					chatids.add(String.valueOf(chatid));
			        				}
		        				}
		        			}

		        			String chatlist = String.join("-", chatids);
		        			
		        			final convlist msg = new convlist(id,"listconv", userid, chatlist);
		        			final String json = gson.toJson(msg);
		        			conn.send(json);
		        			
		        		    } catch(SQLException e) {
		        		    	e.printStackTrace();
		        		    }
		        		} else {
		        			final error msg = new error(id,"error",257);
		        			final String json = gson.toJson(msg);
		        			conn.send(json);
		        		}
		            } else if(type.equals("fetchusers")) {
		            	
		            	name = reader.nextName();
		            	
		            	String useridlist = reader.nextString();
		        		
		        		String[] userids = useridlist.split("-");

		        		List<String> usernames = new ArrayList<String>();

		        		for(int i = 0; i < userids.length; i++) {
		        			try {
		        				String requeteuserdata = "SELECT username FROM user WHERE userid = '" + userids[i] + "'";
			        			Statement stateid = con.createStatement();
			        			ResultSet sid;
			        			sid = stateid.executeQuery(requeteuserdata);
			        			sid.next();
			        			String receivedusernames = sid.getString("username");
			        			usernames.add(String.valueOf(receivedusernames));
			        			
		        			} catch(SQLException e) {
		        				e.printStackTrace();
		        			}			
		        		}
		        		final userdata msg = new userdata(id,"userdata", usernames);
	        			final String json = gson.toJson(msg);
	        			conn.send(json);
	
		            } else if(type.equals("fetchchatid")) {
		            	
		            	try {
		            		
		            		name = reader.nextName();
		            		
		            		String receivedchatid = reader.nextString();
		            		
		            		String[] chatids = receivedchatid.split("-");
		            		
		            		HashMap<String, String> Chatlist  = new HashMap<String, String>();
		            		
		            		for(int i = 0; i < chatids.length; i++) {
		            			String requetechatid = "SELECT userid FROM conversation WHERE chatid = '" + chatids[i] + "'";
	    						Statement stateid = con.createStatement();
	    						ResultSet sid;
	    						
	    						sid = stateid.executeQuery(requetechatid);
	    						
	    						if (sid.next()) {
	    		        			String userids = sid.getString("userid");
	    		        			
	    		        			Chatlist.put(chatids[i], userids);
	    		        			
	    						} 
	    						
		            		}

        					final fetchchatid msg = new fetchchatid(id,"fetchchatid", Chatlist);
    		        		final String json = gson.toJson(msg);
    		        		conn.send(json);

		            	} catch(SQLException e) {
		            		e.printStackTrace();
		            	}
		            	
		            } else if(type.equals("message")) {
		        			
		            	name = reader.nextName();
	        			int receiveduserid = reader.nextInt();
	        			name = reader.nextName();
	        			int receivedchatID = reader.nextInt();
	        			name=reader.nextName();
	        			String receivedMessage = reader.nextString();
	        			
	        			boolean correctuser = false;
	        			
	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}
	        			
	        			
		        		if(isLogged && correctuser) {

		        			try {
		        				
		        			
		        			String requetechatid = "SELECT userid FROM conversation WHERE chatid = '" + receivedchatID + "'";
    						Statement stateid = con.createStatement();
    						ResultSet sid;
    						
    						sid = stateid.executeQuery(requetechatid);
    						
    						sid.next();
    						
    						String[] userids = sid.getString("userid").split("-");
    						
    						System.out.println(Arrays.asList(Connections));
    						
    						
    						String request = "INSERT INTO `" + receivedchatID + "`(timestamp,userid,message) VALUES ('" + getCurrentTimeStamp() + "','" + receiveduserid + "','" + receivedMessage + "')";
    		        		Statement stat = con.createStatement();
    		        	    stat.executeUpdate(request);
    						
    						
    		        		for(int i = 0; i < userids.length; i++) {
    		        			System.out.println(userids[i]);
    		        			if (Connections.containsKey(Integer.parseInt(userids[i]))) {
    		        				final msg msg = new msg(id,"receivedmessage",receivedchatID, receiveduserid, receivedMessage);
        		        			final String json = gson.toJson(msg);
    		        				Connections.get(Integer.parseInt(userids[i])).send(json);
    		        			}
    		        		}

    						
		        			} catch(SQLException e ) {
		        				e.printStackTrace();
		        			}
		        			
		        			
		        		} else {
		        			final error msg = new error(id,"error",256);
		        			final String json = gson.toJson(msg);
		        			conn.send(json);
		        			
		        		}
		        	} else if(type.equals("changemail")) {
		        		
		        		name = reader.nextName();
		        		int receiveduserid = reader.nextInt();
		        		name = reader.nextName();
		        		String oldmail = reader.nextString();
		        		name = reader.nextName();
		        		String newmail = reader.nextString();
		        		
		        		boolean correctuser = false;
	        			
	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}
	        			
	        			if(isLogged && correctuser) {
	        				
	        				try {
	        					String request = "UPDATE user SET email = '" + newmail + "' WHERE email = '" + oldmail + "'";
	    		        		Statement stat = con.createStatement();
	    		        	    stat.executeUpdate(request);
	    		        	    
	    		        	    final hello msg = new hello(id,"success");
	    				        final String json = gson.toJson(msg);
	    				        conn.send(json);
	    				        
	        				} catch(SQLException e) {
	        					e.printStackTrace();
	        				}
	        				
	        			}
	        			
		        		
		        	} else if(type.equals("changepassword")) {

		        		name = reader.nextName();
		        		int receiveduserid = reader.nextInt();
		        		name = reader.nextName();
		        		String oldpassword = reader.nextString();
		        		name = reader.nextName();
		        		String newpassword = reader.nextString();
		        		
		        		boolean correctpassword = false;
		        		
		        		try {
		        			
		        			
		        			String requetechatid = "SELECT password FROM user WHERE userid = '" + receiveduserid + "'";
    						Statement stateid = con.createStatement();
    						ResultSet sid;
    						
    						sid = stateid.executeQuery(requetechatid);
    						
    						sid.next();
		        			
    						String sqlpass = sid.getString("password");
    						
    						if(sqlpass.equals(oldpassword)) {
    							
    							correctpassword = true;
    						}
		        			
		        		} catch(SQLException e) {
		        			e.printStackTrace();
		        		}
		        		
		        		
		        		
		        		boolean correctuser = false;
	        			
	        			for (Entry<Integer, WebSocket> entry : Connections.entrySet()) {
	        	            if (entry.getValue().equals(conn)) {
	        	            	userid = entry.getKey();
	        	            	if(receiveduserid == userid) {
	        	            		correctuser = true;
	        	            	}
	        	            }
	        			}
	        			
	        			
	        			
	        			if(isLogged && correctuser && correctpassword) {
	        				
	        				try {
	        					String request = "UPDATE user SET password = '" + newpassword + "' WHERE userid = '" + receiveduserid + "'";
	    		        		Statement stat = con.createStatement();
	    		        	    stat.executeUpdate(request);
	    		        	    
	    		        	    final hello msg = new hello(id,"success");
	    				        final String json = gson.toJson(msg);
	    				        conn.send(json);
	        				} catch(SQLException e) {
	        					e.printStackTrace();
	        				}
	        				
	        			} else {
	        				final error msg = new error(id,"error",456);
		        			final String json = gson.toJson(msg);
		        			conn.send(json);
	        			}
		        	} else if(type.equals("fetchfriend")) {
		            	
		            	name = reader.nextName();
		            	
		            	String useridlist = reader.nextString();
		        		
		        		String[] userids = useridlist.split("-");

		        		List<String> friends = new ArrayList<String>();

		        		for(int i = 0; i < userids.length; i++) {
		        			try {
		        				String requetefriend = "SELECT friend FROM user WHERE userid = '" + userids[i] + "'";
			        			Statement stateid = con.createStatement();
			        			ResultSet sid;
			        			sid = stateid.executeQuery(requetefriend);
			        			sid.next();
			        			String receivedfriends = sid.getString("username");
			        			friends.add(String.valueOf(receivedfriends));
			        			
		        			} catch(SQLException e) {
		        				e.printStackTrace();
		        			}

		        			
		        		}
		        		final userdata msg = new userdata(id,"friend", friends);
	        			final String json = gson.toJson(msg);
	        			conn.send(json);
		        	} else if(type.equals("fetchmessage")) {
		        		
		        		name = reader.nextName();
		        		int receivedChatId = reader.nextInt();
		        		name = reader.nextName();
		        		String messagesCount = reader.nextString();
		        		String[] messagescounts = messagesCount.split("-");
		        		
		        		int minMessages = Integer.parseInt(messagescounts[0]);
		        		int maxMessages = Integer.parseInt(messagescounts[1]);
		        		
		        		HashMap<Integer,List<List<String>>> globallist = new HashMap<Integer, List<List<String>>>();
		        		
		        		try {
		        			
		        			String requetelastmessage = "SELECT * FROM `" + receivedChatId + "` ORDER BY messageid DESC LIMIT 1";
		        			Statement stateid = con.createStatement();
		        			ResultSet sid;
		        			sid = stateid.executeQuery(requetelastmessage);
		        			int lastmessage = 0;
		        			
		        			while(sid.next()) {
		        				lastmessage = sid.getInt("messageid");
		        			}
		        			
		        			System.out.println(minMessages);
		        			int startingmessage = lastmessage - minMessages;
		        			
		        			System.out.println(startingmessage);
		        			
		        			
		        			if(maxMessages > lastmessage) {
		        				final fetchmaxmessages msg = new fetchmaxmessages(id,"fetchmessage",receivedChatId, lastmessage);
		 				        final String json = gson.toJson(msg);
		 				        conn.send(json);
		        			} else {
		        				for(int i = startingmessage; i > lastmessage - maxMessages; i--) {
				        			
		        					System.out.println("i:" + i);
		        					String requeteuserid = "SELECT userid FROM `" + receivedChatId + "`" + " WHERE messageid = '" + i + "'";
			        				String requetetimestamp = "SELECT timestamp FROM `" + receivedChatId + "`" + " WHERE messageid = '" + i + "'";
			        				String requetemessage = "SELECT message FROM `" + receivedChatId + "`" + " WHERE messageid = '" + i + "'";
			        				Statement stateuserid = con.createStatement();
			        				Statement statetimestamp = con.createStatement();
			        				Statement statemessage = con.createStatement();
			        				
				        			ResultSet resultuserid;
				        			ResultSet resulttimestamp;
				        			ResultSet resultmessage;
				        			resultuserid = stateuserid.executeQuery(requeteuserid);
				        			resulttimestamp = statetimestamp.executeQuery(requetetimestamp);
				        			resultmessage = statemessage.executeQuery(requetemessage);
				        			
				        			resultuserid.next();
				        			resulttimestamp.next();
				        			resultmessage.next();
				        			
				        			List<String> list = new ArrayList<String>();
				        			
				        			String timestamp = resulttimestamp.getString("timestamp");
				        			String messages = resultmessage.getString("message");
				        			int useridmessage = resultuserid.getInt("userid");
				        			
				        			list.add(timestamp);
				        			list.add(messages);
				        			
				        			
				        			
				        			if(globallist.containsKey(useridmessage)) {
				        				globallist.get(useridmessage).add(list);
				        			} else {
				        				
				        				
				        				List<List<String>> listes = new ArrayList<List<String>>();
				        				listes.add(list);
				        				
				        				globallist.put(useridmessage, listes);
				        			}   			
		        			}

		        			final fetchmessages msg = new fetchmessages(id,"fetchmessage",receivedChatId, globallist);
	 				        final String json = gson.toJson(msg);
	 				        conn.send(json);
		        			}
		        			
		        				
		        			
		        		} catch(SQLException e) {
		        			e.printStackTrace();
		        		}
		        		
		        		
		        		
		        		
		        	}
		        }        
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		}			
	}


	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if( conn != null ) {
			// some errors like port binding failed may not be assignable to a specific websocket
		}
	}

	public void onStart() {
		System.out.println("Server started!");
		
		try {
	         Class.forName("com.mysql.cj.jdbc.Driver");
	      } catch (ClassNotFoundException e) {
	    	  e.printStackTrace();
	      }
		
		try {
	    	con=DriverManager.getConnection("jdbc:mysql://chat.nathanweb.dyjix.fr:3306/bddchat?serverTimezone=" + TimeZone.getDefault().getID() + "&useUnicode=yes&character_set_server=utf8mb4&characterEncoding=UTF-8","bddchat","wA9aq_85");
	    	Statement stmt = con.createStatement();
	        stmt.executeQuery("SET NAMES utf8mb4");
	        
	    } catch(SQLException e) {
	    	e.printStackTrace();
	    }
		
	}
	
	public static long getCurrentTimeStamp() {

		Date today = new Date();
		return today.getTime();

	}
	
	
	public static class message {
		public static class hello {
			private final int id;
			private final String type;
			
			public hello(final int id, final String type) {
				super();
				this.id = id;
				this.type = type;
			}
			
			public int getId() {
				return this.id;
			}
			
			public String getType() {
				return this.type;
			}
			
			
			@Override
			public String toString() {
				return "message [id=" + this.id + "type: " + this.type + "]";
			}
			
		}
		public static class error {
			private final int id;
			private final String type;
			private final int errorid;
			
			public error(final int id, final String type, final int errorid) {
				super();
				this.id = id;
				this.type = type;
				this.errorid = errorid;
			}
			
			public int getId() {
				return this.id;
			}
			
			public String getType() {
				return this.type;
			}
			
			public int getErrorId() {
				return this.errorid;
			}
			
			@Override
			public String toString() {
				return "message [id=" + this.id + "type: " + this.type + "erreur:" + this.errorid + "]";
			}
			}
			
			public static class login {
				private final int id;
				private final String type;
				private final int userId;
				private final String username;
				
				public login(final int id, final String type, final int userId, final String username) {
					super();
					this.id = id;
					this.type = type;
					this.userId = userId;
					this.username = username;
				}
				
				public int getId() {
					return this.id;
				}
				
				public String getType() {
					return this.type;
				}
				
				public int getUserId() {
					return this.userId;
				}
				
				public String getUsername() {
					return this.username;
				}
				
				@Override
				public String toString() {
					return "message [id=" + this.id + "type: " + this.type + "userid" + this.userId + "username" + this.username + "]";
				}
			    }
				public static class convlist {
					private final int id;
					private final String type;
					private final int userid;
					private final String chatlist;
					
					public convlist(final int id, final String type, final int userid, final String chatlist) {
						super();
						this.id = id;
						this.type = type;
						this.userid = userid;
						this.chatlist = chatlist;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public int getUserId() {
						return this.userid;
					}
					
					public String getchatlist() {
						return this.chatlist;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "userid" + this.userid + "conv" + this.chatlist + "]";
					}
				}
				public static class chatid {
					private final int id;
					private final String type;
					private final int chatid;
					
					public chatid(final int id, final String type, final int chatid) {
						super();
						this.id = id;
						this.type = type;
						this.chatid = chatid;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public int getChatId() {
						return this.chatid;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "chatid" + this.chatid + "]";
					}
				}
				public static class userdata {
					private final int id;
					private final String type;
					private final List<String> usernameList;
					
					public userdata(final int id, final String type, final List<String> usernameList) {
						super();
						this.id = id;
						this.type = type;
						this.usernameList = usernameList;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public List<String> getusernamelist() {
						return this.usernameList;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "usernames" + this.usernameList + "]";
					}
				}
				public static class fetchchatid {
					private final int id;
					private final String type;
					private final HashMap<String, String>chatlist;
					
					public fetchchatid(final int id, final String type, final HashMap<String, String>chatlist ) {
						super();
						this.id = id;
						this.type = type;
						this.chatlist = chatlist;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public HashMap<String, String> getchatlist() {
						return this.chatlist;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "chatlist" + this.chatlist + "]";
					}
				}
				
				public static class msg{
					private final int id;
					private final String type;
					private final int chatid;
					private final int userid;
					private final String message;
					
					
					public msg(final int id, final String type, final int chatid, final int userid, final String message) {
						super();
						this.id = id;
						this.type = type;
						this.chatid = chatid;
						this.userid = userid;
						this.message = message;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public int getchatid() {
						return this.chatid;
					}
					
					public int getuserid() {
						return this.userid;
					}
					
					public String getMessage() {
						return this.message;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "chatid" + this.chatid + "userid" + this.userid + "message" + this.message +"]";
					}
				}
				
				public static class fetchmessages {
					private final int id;
					private final String type;
					private final int chatid;
					private final HashMap<Integer, List<List<String>>>messageList;
					
					public fetchmessages(final int id, final String type, final int chatid ,final HashMap<Integer, List<List<String>>>messageList ) {
						super();
						this.id = id;
						this.type = type;
						this.chatid = chatid;
						this.messageList = messageList;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public int getChatid() {
						return this.chatid;
					}
					
					
					public HashMap<Integer, List<List<String>>> getchatlist() {
						return this.messageList;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "chatid: " + this.chatid + "messageList" + this.messageList + "]";
					}
				}
				public static class fetchmaxmessages {
					private final int id;
					private final String type;
					private final int chatid;
					private final int maxmessages;
					
					public fetchmaxmessages(final int id, final String type, final int chatid ,final int maxmessages) {
						super();
						this.id = id;
						this.type = type;
						this.chatid = chatid;
						this.maxmessages = maxmessages;
					}
					
					public int getId() {
						return this.id;
					}
					
					public String getType() {
						return this.type;
					}
					
					public int getChatid() {
						return this.chatid;
					}
					
					
					public int getmaxmessages() {
						return this.maxmessages;
					}
					
					@Override
					public String toString() {
						return "message [id=" + this.id + "type: " + this.type + "chatid: " + this.chatid + "maxmessage" + this.maxmessages + "]";
					}
				}
				
			}
} 